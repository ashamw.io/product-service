package com.asha.productService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asha.productService.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
